// User Schema/Model
const userSchema = new mongoose.Schema({
    username : String,
    password : String
})

// The variable/object "User" can now used to run commands for interacting with our database
const User = mongoose.model("User", userSchema);

// Registering a user

// Business Logic
/*
1. Add a functionality to check if there are duplicate tasks
  - If the user already exists in the database, we return an error
  - If the user doesn't exist in the database, we add it in the database
2. The user data will be coming from the request's body
3. Create a new User object with a "username" and "password" fields/properties
*/

// Route for creating a user
app.post("/signup", (req, res)=> {

  // Finds for a document with the matching username provided in the client/Postman
  User.findOne({ username : req.body.username }).then((result, err) => {

    // Check for duplicates
    if(result != null && result.username == req.body.username){

      return res.send("Duplicate username found");

    // No duplicates found
    } else {

      // If the username and password are both not blank
      if(req.body.username !== '' && req.body.password !== ''){

        // Create/instantiate a "newUser" from the "User" model
                let newUser = new User({
                    username : req.body.username,
                    password : req.body.password
                });
    
          // Create a document in the database
                newUser.save().then((savedTask, saveErr) => {

                    // If an error occurred
                    if(saveErr){

                      // Return an error in the client/Postman
                        return console.error(saveErr);

                    // If no errors are found
                    } else {

                      // Send a response back to the client/Postman of "created"
                        return res.status(201).send("New user registered");

                    }

                })

            // If the "username" or "password" was left blank
            } else {

              /// Send a response back to the client/Postman of "created"
                return res.send("BOTH username and password must be provided.");
            }     
    }
  })
})

app.listen(port, () => console.log(`Server is running at port ${port}`));
